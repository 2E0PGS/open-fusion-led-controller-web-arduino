/*
###################################################
Open Fusion LED Controller Project.
Designed with IRFZ44N MOSFET's / Power Transistors.
Code written by: Peter Stevenson (2E0PGS).
Code used from my older LED controller program.
Code also used from my Serial to I2C program.
Intended for use with Arduino Nano / Uno.
Main Repository: https://bitbucket.org/2E0PGS/open-fusion-led-controller-main
Web Arduino Repository: https://bitbucket.org/2E0PGS/open-fusion-led-controller-web-arduino
###################################################

Copyright (C) <2016>  <Peter Stevenson> (2E0PGS)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

//-----------------------ETHERNET-SETTINGS-----------------------//
#include <SPI.h>
#include <Ethernet.h>
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0x27 };
byte ip[] = { 192, 168, 0, 21 };
EthernetServer server(80);  // create a server at port 80
/*
Pins used
SPI Bus via ISP header.
This is on digital pins 10, 11, 12, and 13 on the Uno.
Pin 4 for the SD card.
*/
String HTTP_req; // stores the HTTP request
//----------END----------ETHERNET-SETTINGS----------END----------//

#include <Wire.h>

//--------------------Variables--------------------//
int dataToSend = 1; //The variable that stores the data to send.
bool ignoreRequest = 0;
//--------------------Variables-----------------END//

void setup() {
  Wire.begin(); //inital wire bus
  Serial.begin(9600); //inital serial communication

  Ethernet.begin(mac, ip); // initialize Ethernet device
  server.begin(); // start to listen for clients
  
  digitalWrite(13, LOW);
}

void loop() {   
//--------------------HTTP SERVER CODE FOR WEBPAGE--------------------//
// Create a client connection
  EthernetClient client = server.available();
  if (client) {
    while (client.connected()) {   
      if (client.available()) {
        char c = client.read();
     
        //read char by char HTTP request
        if (HTTP_req.length() < 100) {
          //store characters to string
          HTTP_req += c;
          //Serial.print(c);
         }

         //if HTTP request has ended
         if (c == '\n') {          
           Serial.println(HTTP_req); //print to serial monitor for debuging
           client.println("HTTP/1.1 200 OK");
           client.println("Content-Type: text/html");
           client.println("Connection: close");
           client.println();
           // send web page
           client.println("<!DOCTYPE html>");
           client.println("<html>");
           client.println("<head>");
           client.println("<title>Arduino LED Lights Control</title>");
           client.println("</head>");
           client.println("<body>");
           client.println("<h1>Arduino LED Light Strip Controller 2E0PGS Shack</h1>");
           client.println("<p>Click buttons to control the lights</p>");
           client.println("<form method=\"get\">");
           client.println("<a href=\"/?button1on\"\">Turn ON</a>");
           client.println("<a href=\"/?button1off\"\">Turn OFF</a><br />");
           client.println("<a href=\"/?button2on\"\">Dumb LED Mode</a>");
           client.println("<a href=\"/?button3on\"\">Smart LED Mode</a><br />");
           client.println("<hr>");
           client.println("<a href=\"/?button4on\"\">AUTO LDR</a><br />");
           client.println("<a href=\"/?button5on\"\">Jump 3</a><br />"); 
           client.println("<a href=\"/?button6on\"\">Jump 7</a><br />");
           client.println("<a href=\"/?button7on\"\">Fade 3</a><br />"); 
           client.println("<a href=\"/?button8on\"\">Fade White</a><br />");
           client.println("<a href=\"/?button9on\"\">Colour Red</a><br />");
           client.println("<a href=\"/?button10on\"\">Colour Green</a><br />");
           client.println("<a href=\"/?button11on\"\">Colour Blue</a><br />");
           client.println("<a href=\"/?button12on\"\">Colour White</a><br />");
           client.println("<a href=\"/?button13on\"\">Pass Through</a><br />");
           client.println("<hr>");
           client.println("<p>These functions requite Smart LED Mode</p>");
           client.println("<a href=\"/?button14on\"\">RandomDroplet</a><br />");                                       
           client.println("</form>");
           client.println("</body>");
           client.println("</html>");           
           Serial.print(HTTP_req);
           
           delay(1); 
           
           client.stop();

           ignoreRequest = 0;
           
           if (HTTP_req.indexOf("button1on") > 0) {
            dataToSend = 1;
           }
           else if (HTTP_req.indexOf("button1off") > 0) {
            dataToSend = 2;
           }
           else if (HTTP_req.indexOf("button2on") > 0) {
            dataToSend = 3;
           }
           else if (HTTP_req.indexOf("button3on") > 0) {
            dataToSend = 4;
           }
           else if (HTTP_req.indexOf("button4on") > 0) {
            dataToSend = 5;
           }
           else if (HTTP_req.indexOf("button5on") > 0) {
            dataToSend = 6;
           }
           else if (HTTP_req.indexOf("button6on") > 0) {
            dataToSend = 7;
           }
           else if (HTTP_req.indexOf("button7on") > 0) {
            dataToSend = 8;
           }
           else if (HTTP_req.indexOf("button8on") > 0) {
            dataToSend = 9;
           }
           else if (HTTP_req.indexOf("button9on") > 0) {
            dataToSend = 10;
           }           
           else if (HTTP_req.indexOf("button10on") > 0) {
            dataToSend = 11;
           }
           else if (HTTP_req.indexOf("button11on") > 0) {
            dataToSend = 12;
           }
           else if (HTTP_req.indexOf("button12on") > 0) {
            dataToSend = 13;
           }
           else if (HTTP_req.indexOf("button13on") > 0) {
            dataToSend = 14;
           }
           else if (HTTP_req.indexOf("button14on") > 0) {
            dataToSend = 15;
           }

           else { //If we get a get request which is blank or unknown then ignore it.
            ignoreRequest = 1;                        
           }
           
           if (HTTP_req.indexOf("favicon.ico") > 0) {
            ignoreRequest = 1;
           }
           
           if (ignoreRequest == 0){
            sendData(); //Call subroutine to send new dataToSend via I2C.
           }
                      
           //clearing string for next read
           HTTP_req=""; 
         }
       } //END of client available
     } //END of client connected
   } //END of if client
//--------------------HTTP SERVER CODE FOR WEBPAGE-----------------END//
} //END OF VOID LOOP

void sendData() {
  digitalWrite(13, HIGH);
  Wire.beginTransmission(7); //transmit to device with id 7
  Wire.write(dataToSend);
  Serial.println(dataToSend);
  Wire.endTransmission(); // stop transmitting
  digitalWrite(13, LOW);
}


